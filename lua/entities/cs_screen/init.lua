-- init.lua

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")

util.AddNetworkString("show_cash")
util.AddNetworkString("send_to_server")
util.AddNetworkString("send_to_clients")

function ENT:Initialize()
	self:SetModel("models/props_c17/cashregister01a.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)

	local phys = self:GetPhysicsObject()
	phys:Wake()
end

function ENT:Use(ply)
	if (ply:IsPlayer()) then

		net.Start("show_cash")
		net.WriteEntity(self)
		net.Send(ply)

		return ""
	end
end

net.Receive("send_to_server", function()
	local ent = net.ReadEntity()

	ent:SetNWString("LabelText", net.ReadString())
	ent:SetNW("LabelColor", net.ReadTable())
	ent:SetVar("Articles", net.ReadTable())

	net.Start("send_to_clients")
	net.WriteEntity(ENT)
	net.Broadcast()
end)
