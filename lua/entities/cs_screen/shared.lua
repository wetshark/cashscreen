-- shared.lua

ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = "Checkout System"
ENT.Author = "Kaoff & Spectrum"
ENT.LabelText = "Checkout"
ENT.LabelColor = Color(0, 150, 0, 255)
ENT.CashOwner = nil

ENT.Articles = {}

ENT.Spawnable = true
ENT.AdminSpawnable = true