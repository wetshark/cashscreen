-- cl_init.lua

include("shared.lua")

// Creating Fonts
surface.CreateFont("CV200",{
		font = "Coolvetica",
		size = 200,
	})

surface.CreateFont("DD20",{
		font = "Default",
		size = 20,
	})

// --- DrawList Function - Draws List on the DFrame ---
function DrawList(listview, articles)
	
	listview:Clear()

	for k, v in pairs(articles) do
		listview:AddLine(v[1], v[2])
	end

end

// --- DrawCash Function - Draws a DFrame of the used Cash Screen ---
function DrawCash(ply)

	local width = 600
	local height = 500
	local entity = net.ReadEntity()
	local labelcolor = entity.LabelColor

	-- EDIT FRAME ----------------------------------------------------------------------------------------------

	// Edit Frame basis
	local edit_frame = vgui.Create("DFrame")

	edit_frame:SetSize(width, height)
	edit_frame:SetTitle("")
	edit_frame:SetVisible(true)
	edit_frame:Center()
	edit_frame:SetBackgroundBlur(true)
	edit_frame:SetDraggable(false)
	edit_frame:ShowCloseButton(false)
	edit_frame:MakePopup()

	function edit_frame:Paint(w, h)
		draw.RoundedBox(8, 0, 0, w, h, color_white)
		draw.RoundedBoxEx(8, 0, 0, w, 20, Color(200, 0, 0, 255), true, true, false, false)
		draw.SimpleText("Checkout System v0.1", "Default", 6, 4, color_white, 0, 0)
	end

	// Close Button
	local but_close = vgui.Create("DButton", edit_frame)

	but_close:SetPos(570, 0)
	but_close:SetSize(30, 20)
	but_close:SetText("X")
	but_close:SetTextColor(color_white)

	function but_close:Paint(w,h) end

	// Name Text Entry
	local set_text = vgui.Create("DTextEntry", edit_frame)

	set_text:SetPos(10, 30)
	set_text:SetSize(540, 30)
	set_text:SetFont("DD20")
	set_text:SetText(entity.LabelText)

	but_close.DoClick = function()
		edit_frame:Remove()
	end

	// Set Color Button
	local set_color = vgui.Create("DButton", edit_frame)

	set_color:SetSize(30, 30)
	set_color:SetPos(560, 30)
	set_color:SetText("")
	set_color:SetColor(entity.LabelColor)

	function set_color:Paint(w,h)
		draw.RoundedBox(0, 0, 0, w, h, labelcolor)
	end

	// Article List
	local article_list = vgui.Create("DListView", edit_frame)

	article_list:SetPos(10, 70)
	article_list:SetSize(580, 340)
	article_list:SetMultiSelect(false)
	
	local col_article = article_list:AddColumn("Article")
	local col_price = article_list:AddColumn("Price")

	col_price:SetMinWidth(100)
	col_price:SetMaxWidth(100)

	DrawList(article_list, entity.Articles)

	// Add Article Button
	local but_add = vgui.Create("DButton", edit_frame)

	but_add:SetPos(10, edit_frame:GetTall() - 80)
	but_add:SetSize(80, 30)
	but_add:SetText("Add Article")
	but_add:SetTextColor(color_white)

	function but_add:Paint(w,h)
		draw.RoundedBox(5, 0, 0, w, h, Color(0, 100, 0, 255))
	end

	// Del Article Button
	local but_del = vgui.Create("DButton", edit_frame)

	but_del:SetSize(80, 30)
	but_del:SetPos(edit_frame:GetWide() - but_del:GetWide() - 10, edit_frame:GetTall() - 80)
	but_del:SetText("Delete Article")
	but_del:SetTextColor(color_white)

	function but_del:Paint(w,h)
		draw.RoundedBox(5, 0, 0, w, h, Color(100, 0, 0, 255))
	end

	// Edit Article Button
	local but_edit = vgui.Create("DButton", edit_frame)

	but_edit:SetSize(80, 30)
	but_edit:SetPos(edit_frame:GetWide() - but_edit:GetWide() - but_del:GetWide() - 20, edit_frame:GetTall() - 80)
	but_edit:SetText("Edit Article")
	but_edit:SetTextColor(color_white)

	function but_edit:Paint(w,h)
		draw.RoundedBox(5, 0, 0, w, h, Color(0, 0, 100, 255))
	end

	// Send Button
	local but_send = vgui.Create("DButton", edit_frame)

	but_send:SetPos(10, edit_frame:GetTall() - 40)
	but_send:SetSize(580, 30)
	but_send:SetText("Send")
	but_send:SetTextColor(color_white)

	function but_send:Paint(w,h)
		draw.RoundedBox(5, 0, 0, w, h, Color(200, 0, 0, 255))
	end
	
	but_send.DoClick = function()
		local text_entry = set_text:GetValue()

		if (#text_entry <= 50) then
			entity.LabelText = text_entry
		else
			-- Text can't be more than 50 caracters
			notification.AddLegacy("WARNING : The text is too long ! ( > 50 )", NOTIFY_ERROR, 5)
			surface.PlaySound("buttons/button10.wav")
		end

		entity.LabelColor = labelcolor

		net.Start("send_to_server")
			net.WriteEntity(entity)
			net.WriteString(entity.LabelText)
			net.WriteTable(entity.LabelColor)
			net.WriteTable(entity.Articles)
		net.SendToServer()

		edit_frame:Remove()	
	end

	-- NO SELECTION ERROR --------------------------------------------------------------------------------

	local sel_err_frame = vgui.Create("DFrame", edit_frame)

	sel_err_frame:SetSize(400, 110)
	sel_err_frame:SetPos(edit_frame:GetWide()/2 - sel_err_frame:GetWide()/2, edit_frame:GetTall()/2 - sel_err_frame:GetTall()/2)
	sel_err_frame:ShowCloseButton(false)
	sel_err_frame:SetVisible(false)
	sel_err_frame:SetTitle("")

	function sel_err_frame:Paint(w, h)
		draw.RoundedBox(4, 0, 0, w, h, Color(200, 200, 200))
		draw.RoundedBoxEx(4, 0, 0, w, 20, Color(200, 0, 0, 255), true, true, false, false)
		draw.SimpleText("ERROR : No Selection", "Default", 6, 4, color_white, 0, 0)
	end

	// ERROR Label
	local sel_err_label = vgui.Create("DLabel", sel_err_frame)

	sel_err_label:SetText("WARNING -- Select an item before that !")
	sel_err_label:SetPos(10, 20)
	sel_err_label:SetFont("DD20")
	sel_err_label:SetTextColor(color_black)
	sel_err_label:SetSize(sel_err_frame:GetWide() - 20, 40)

	// ERROR OK Button
	local ok_selerr_btn = vgui.Create("DButton", sel_err_frame)

	ok_selerr_btn:SetPos(10, sel_err_frame:GetTall() - 40)
	ok_selerr_btn:SetSize(sel_err_frame:GetWide() - 20, 30)
	ok_selerr_btn:SetText("Ok")
	ok_selerr_btn:SetTextColor(color_white)

	function ok_selerr_btn:Paint(w,h)
		draw.RoundedBox(5, 0, 0, w, h, Color(200, 0, 0, 255))
	end

	ok_selerr_btn.DoClick = function()
		sel_err_frame:SetVisible(false)
	end

	-- ADD ARTICLE FRAME ---------------------------------------------------------------------------------

	// Add Frame Basis
	local add_frame = vgui.Create("DFrame", edit_frame)

	add_frame:SetSize(400, 110)
	add_frame:SetPos(edit_frame:GetWide()/2 - add_frame:GetWide()/2, edit_frame:GetTall()/2 - add_frame:GetTall()/2)
	add_frame:ShowCloseButton(false)
	add_frame:SetVisible(false)
	add_frame:SetTitle("")

	function add_frame:Paint(w, h)
		draw.RoundedBox(4, 0, 0, w, h, Color(200, 200, 200))
		draw.RoundedBoxEx(4, 0, 0, w, 20, Color(200, 0, 0, 255), true, true, false, false)
		draw.SimpleText("Add a new article", "Default", 6, 4, color_white, 0, 0)
	end

	// Add Frame Close Button
	local add_but_close = vgui.Create("DButton", add_frame)

	add_but_close:SetPos(370, 0)
	add_but_close:SetSize(30, 20)
	add_but_close:SetText("X")
	add_but_close:SetTextColor(color_white)

	function add_but_close:Paint(w,h) end

	add_but_close.DoClick = function()
		add_frame:SetVisible(false)
	end

	// Add Frame Text Entry
	local art_text = vgui.Create("DTextEntry", add_frame)

	art_text:SetPos(10, 30)
	art_text:SetSize(290, 30)
	art_text:SetFont("DD20")
	art_text:SetText("Article Name")

	// Add Frame Price Entry
	local art_price = vgui.Create("DNumberWang", add_frame)

	art_price:SetPos(art_text:GetWide() + 20, 30)
	art_price:SetSize(add_frame:GetWide() - art_text:GetWide() - 30, 30)
	art_price:SetFont("DD20")
	art_price:SetMax(9999999999)

	// Add Frame Send Button
	local add_but_send = vgui.Create("DButton", add_frame)

	add_but_send:SetPos(10, add_frame:GetTall() - 40)
	add_but_send:SetSize(add_frame:GetWide() - 20, 30)
	add_but_send:SetText("Send")
	add_but_send:SetTextColor(color_white)

	function add_but_send:Paint(w,h)
		draw.RoundedBox(5, 0, 0, w, h, Color(200, 0, 0, 255))
	end

	add_but_send.DoClick = function()
		table.insert(entity.Articles, {art_text:GetValue(), art_price:GetValue()})
		DrawList(article_list, entity.Articles)
		add_frame:SetVisible(false)
	end

	// Parent Frame Add Button DoClick function
	but_add.DoClick = function()
		art_text:SetText("Article Name")
		art_price:SetValue(0)
		add_frame:SetVisible(true)
	end


	-- DELETE ARTICLE FRAME ------------------------------------------------------------------------------

	// Delete Frame Basis
	local del_frame = vgui.Create("DFrame", edit_frame)

	del_frame:SetSize(400, 110)
	del_frame:SetPos(edit_frame:GetWide()/2 - del_frame:GetWide()/2, edit_frame:GetTall()/2 - del_frame:GetTall()/2)
	del_frame:ShowCloseButton(false)
	del_frame:SetVisible(false)
	del_frame:SetTitle("")

	function del_frame:Paint(w, h)
		draw.RoundedBox(4, 0, 0, w, h, Color(200, 200, 200))
		draw.RoundedBoxEx(4, 0, 0, w, 20, Color(200, 0, 0, 255), true, true, false, false)
		draw.SimpleText("Confirm delete ?", "Default", 6, 4, color_white, 0, 0)
	end

	// Delete Label
	local del_label = vgui.Create("DLabel", del_frame)

	del_label:SetText("Are you sure you want to delete ?")
	del_label:SetPos(10, 20)
	del_label:SetFont("DD20")
	del_label:SetTextColor(color_black)
	del_label:SetSize(del_frame:GetWide() - 20, 40)

	// Delete YES Button
	local yes_del_btn = vgui.Create("DButton", del_frame)

	yes_del_btn:SetPos(10, add_frame:GetTall() - 40)
	yes_del_btn:SetSize(add_frame:GetWide() / 2 - 15, 30)
	yes_del_btn:SetText("Yes")
	yes_del_btn:SetTextColor(color_white)

	function yes_del_btn:Paint(w,h)
		draw.RoundedBox(5, 0, 0, w, h, Color(0, 100, 0, 255))
	end

	yes_del_btn.DoClick = function()
		table.remove(entity.Articles, article_list:GetSelectedLine())
		DrawList(article_list, entity.Articles)
		del_frame:SetVisible(false)
	end

	// Delete NO Button
	local no_del_btn = vgui.Create("DButton", del_frame)

	no_del_btn:SetPos(yes_del_btn:GetWide() + 20, add_frame:GetTall() - 40)
	no_del_btn:SetSize(add_frame:GetWide() / 2 - 15, 30)
	no_del_btn:SetText("No")
	no_del_btn:SetTextColor(color_white)

	function no_del_btn:Paint(w,h)
		draw.RoundedBox(5, 0, 0, w, h, Color(100, 0, 0, 255))
	end

	no_del_btn.DoClick = function()
		del_frame:SetVisible(false)
	end

	// Parent Frame Del Button DoClick function
	but_del.DoClick = function()
		if (article_list:GetSelectedLine()) then
			local index = article_list:GetSelectedLine()

			if index > 0 then
				del_label:SetText("Are you sure you want to delete " .. entity.Articles[index][1] .. " ?")
				del_frame:SetVisible(true)
			end
		else
			sel_err_frame:SetVisible(true)
		end
	end

	-- MODIFY ARTICLE FRAME ------------------------------------------------------------------------------

	// Modify Frame Basis
	local mod_frame = vgui.Create("DFrame", edit_frame)

	mod_frame:SetSize(400, 110)
	mod_frame:SetPos(edit_frame:GetWide()/2 - mod_frame:GetWide()/2, edit_frame:GetTall()/2 - mod_frame:GetTall()/2)
	mod_frame:ShowCloseButton(false)
	mod_frame:SetVisible(false)
	mod_frame:SetTitle("")

	function mod_frame:Paint(w, h)
		draw.RoundedBox(4, 0, 0, w, h, Color(200, 200, 200))
		draw.RoundedBoxEx(4, 0, 0, w, 20, Color(200, 0, 0, 255), true, true, false, false)
		draw.SimpleText("Edit Article", "Default", 6, 4, color_white, 0, 0)
	end

	// Add Frame Close Button
	local mod_but_close = vgui.Create("DButton", mod_frame)

	mod_but_close:SetPos(370, 0)
	mod_but_close:SetSize(30, 20)
	mod_but_close:SetText("X")
	mod_but_close:SetTextColor(color_white)

	function mod_but_close:Paint(w,h) end

	mod_but_close.DoClick = function()
		mod_frame:SetVisible(false)
	end

	// Add Frame Text Entry
	local mod_art_text = vgui.Create("DTextEntry", mod_frame)

	mod_art_text:SetPos(10, 30)
	mod_art_text:SetSize(290, 30)
	mod_art_text:SetFont("DD20")
	mod_art_text:SetText("Article Name")

	// Add Frame Price Entry
	local mod_art_price = vgui.Create("DNumberWang", mod_frame)

	mod_art_price:SetPos(mod_art_text:GetWide() + 20, 30)
	mod_art_price:SetSize(add_frame:GetWide() - mod_art_text:GetWide() - 30, 30)
	mod_art_price:SetFont("DD20")
	mod_art_price:SetMax(9999999999)

	// Add Frame Send Button
	local mod_but_send = vgui.Create("DButton", mod_frame)

	mod_but_send:SetPos(10, add_frame:GetTall() - 40)
	mod_but_send:SetSize(add_frame:GetWide() - 20, 30)
	mod_but_send:SetText("Send")
	mod_but_send:SetTextColor(color_white)

	function mod_but_send:Paint(w,h)
		draw.RoundedBox(5, 0, 0, w, h, Color(200, 0, 0, 255))
	end

	mod_but_send.DoClick = function()
		local index = article_list:GetSelectedLine()

		entity.Articles[index][1] = mod_art_text:GetValue()
		entity.Articles[index][2] = mod_art_price:GetValue()

		DrawList(article_list, entity.Articles)
		mod_frame:SetVisible(false)
	end

	// Parent Frame Edit Button
	but_edit.DoClick = function()
		if (article_list:GetSelectedLine()) then
			local index = article_list:GetSelectedLine()

			if index > 0 then
				mod_art_text:SetText(entity.Articles[index][1])
				mod_art_price:SetValue(entity.Articles[index][2])

				mod_frame:SetVisible(true)
			end
		else
			sel_err_frame:SetVisible(true)
		end
	end

	-- COLOR FRAME ---------------------------------------------------------------------------------------

	// Color Frame Basis
	local color_frame = vgui.Create("DFrame", edit_frame)

	color_frame:SetSize(300, 300)
	color_frame:SetPos(edit_frame:GetWide()/2 - color_frame:GetWide()/2, edit_frame:GetTall()/2 - color_frame:GetTall()/2)
	color_frame:ShowCloseButton(false)
	color_frame:SetVisible(false)
	color_frame:SetTitle("")

	function color_frame:Paint(w, h)
		draw.RoundedBox(4, 0, 0, w, h, Color(200, 200, 200))
		draw.RoundedBoxEx(4, 0, 0, w, 20, Color(200, 0, 0, 255), true, true, false, false)
		draw.SimpleText("Change title color", "Default", 6, 4, color_white, 0, 0)
	end

	// Color Frame Close Button
	local color_but_close = vgui.Create("DButton", color_frame)

	color_but_close:SetPos(270, 0)
	color_but_close:SetSize(30, 20)
	color_but_close:SetText("X")
	color_but_close:SetTextColor(color_white)

	function color_but_close:Paint(w,h) end

	color_but_close.DoClick = function()
		color_frame:SetVisible(false)
	end

	// Color Picker
	local color_picker = vgui.Create("DColorMixer", color_frame)

	color_picker:SetPalette(false)
	color_picker:SetWangs(false)
	color_picker:SetAlphaBar(false)
	color_picker:SetSize(280, 220)
	color_picker:SetPos(10, 30)

	// Color Frame Send Button
	local color_but_send = vgui.Create("DButton", color_frame)

	color_but_send:SetPos(10, 300 - 40)
	color_but_send:SetSize(280, 30)
	color_but_send:SetText("Send")
	color_but_send:SetTextColor(color_white)

	function color_but_send:Paint(w,h)
		draw.RoundedBox(5, 0, 0, w, h, Color(200, 0, 0, 255))
	end

	color_but_send.DoClick = function()
		labelcolor = color_picker:GetColor()
		color_frame:SetVisible(false)
	end

	// DFrame set color button sets Color DFrame visible
	set_color.DoClick = function()
		color_frame:SetVisible(true)
	end

end

function ENT:Initialize()
	self.rotate = 0
	self.lasttime = 0
end

function ENT:Draw()
	self:DrawModel()

	local ang = self:GetAngles()
	local ang2 = self:GetAngles()

	local text = self.LabelText or "Caisse Libre"

	local font = "CV200"

	ang:RotateAroundAxis(self:GetAngles():Forward(), -90)
	ang:RotateAroundAxis(self:GetAngles():Right(), 180)
	ang:RotateAroundAxis(self:GetAngles():Up(), self.rotate)

	ang2:RotateAroundAxis(self:GetAngles():Forward(), 90)
	ang2:RotateAroundAxis(self:GetAngles():Up(), self.rotate)

	-- FRONT TEXT
	cam.Start3D2D(self:GetPos(), ang, 0.03)
		draw.SimpleTextOutlined(text, font, 0, -600, self.LabelColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP, 3, Color(0, 0, 0))
	cam.End3D2D()

	-- BACK TEXT
	cam.Start3D2D(self:GetPos(), ang2, 0.03)
		draw.SimpleTextOutlined(text, font, 0, -600, self.LabelColor, TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP, 3, Color(0, 0, 0))
	cam.End3D2D()
end

function ENT:Think()
	if self.rotate > 359 then 
		self.rotate = 0
	end

	self.rotate = self.rotate - (50 * (self.lasttime - SysTime()))
	self.lasttime = SysTime()

end

net.Receive("show_cash", DrawCash)
net.Receive("send_to_clients", function()

	ENT = net.ReadEntity()

end)