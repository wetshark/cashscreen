-- cl_init.lua

include("shared.lua")

function ENT:Initialize()
	self.rotate = 0
	self.lastTime = 0
end

function ENT:Draw()
	self:DrawModel()

	local pos = self:GetPos()
	local ang1 = Angle(0, 90 + self.rotate, 90)
	local ang2 = Angle(0, 270 + self.rotate, 90)

	local font = "DermaDefault"
	local textString = "Caisse Libre"

	local width = surface.GetTextSize(textString)
	local height = draw.GetFontHeight(font)

	-- FRONT TEXT
	cam.Start3D2D(pos + Vector(0, 0, 30), ang1, 0.5)
		draw.RoundedBox( 0, -width/3 -1, 0, width-width/3 +2, height, Color(0, 0, 0, 150))
		draw.SimpleText(textString, font, 0, 0, Color(0, 0 , 255, 255), TEXT_ALIGN_CENTER)
	cam.End3D2D()

	-- BACK TEXT
	cam.Start3D2D(pos + Vector(0, 0, 30), ang2, 0.5)
		draw.SimpleText(textString, font, 0, 0, Color(0, 0 , 255, 255), TEXT_ALIGN_CENTER)
	cam.End3D2D()
end

function ENT:Think()
	if self.rotate > 359 then self.rotate = 0 end

	self.rotate = self.rotate - (0 * (self.lastTime-SysTime()))
	self.lastTime = SysTime()
end